# sops2sealedsecret

This program is designed to reencrypt a file from SOPS to SealedSecret format for use in OpenShift or Kubernetes.

## Why is this useful?

In environments where secrets management is crucial, having a tool to seamlessly convert between formats can save time and reduce errors. This script provides a streamlined way to perform this conversion, ensuring that the user has the necessary permissions and that files are in the correct state before proceeding.

## Installation

Install `sops2sealedsecret` with the command `go install codeberg.org/goern/sops2sealedsecret@latest`. Go will
automatically install it in your $GOPATH/bin directory which should be in your $PATH.

Once installed you should have the command available. Confirm it by typing `sops2sealedsecret version`.

## Usage

```bash
sops2sealedsecret --context nostromo --namespace test fixtures/test.env.yaml b.yaml
```

Optional: Use `--force` to overwrite the output file if it exists.

## Requirements

- `sops`
- `kubeseal`

For more details, refer to the [original prompt provided](https://chat.openai.com/share/2764e69a-cb09-4cee-9f83-6961b50731f8).

## Credits

I took some Makefile-magick from <https://github.com/pasdam/> Thanks!
