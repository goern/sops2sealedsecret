PKG_NAME:=codeberg.org/goern/sops2sealedsecret
BUILD_DIR:=bin
BINARY:=$(BUILD_DIR)/sops2sealedsecret

IMAGE := goern/sops2sealedsecret

VERSION=$(shell git describe --tags --always --dirty)
LDFLAGS=-s -X codeberg.org/goern/sops2sealedsecret/pkg/sops2sealedsecret.Version=$(VERSION) -X main.GITCOMMIT=`git rev-parse --short HEAD`

.PHONY: go-build
go-build: | go-dep-clean go-dep-download
	@go build -v -ldflags "$(LDFLAGS)" -o $(BINARY) *.go

.PHONY: go-dep-clean
go-dep-clean:
	@go mod tidy

.PHONY: go-dep-download
go-dep-download: | go-dep-clean
	@go mod download

.PHONY: go-clean
go-clean:
	@rm -rf $(BUILD_DIR)/
