# CHANGELOG

## v0.2.1 (2023-09-21)

### Refactor

- refactored all the things a little more

## v0.2.0 (2023-09-20)

### Feat

- a golang implementation

### Refactor

- **cz**: fix cz config

## v0.1.1 (2023-09-20)

### Fix

- cz config

## 0.1.1 (2023-09-20)

### Fix

- cz config

## v0.1.0 (2023-09-20)

### Fix

- cz and poetry config

### Refactor

- **black,-flake8**: some reformatting
