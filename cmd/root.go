/*
Copyright © 2023 Christoph Görn <goern@b4mad.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"log"
	"os"

	"codeberg.org/goern/sops2sealedsecret/pkg/helpers"
	"codeberg.org/goern/sops2sealedsecret/pkg/sops2sealedsecret"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	k8s_context string
	namespace   string
	dryRun      bool
	force       bool
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "sops2sealedsecret <fromSopsFile> <toSealedSecretFile>",
	Short: "Reencrypts from SOPS to SealedSecret",
	Args:  cobra.ExactArgs(2),
	Run:   execute,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd.Flags().StringVar(&k8s_context, "context", "", "Kubernetes context (required)")
	rootCmd.Flags().StringVar(&namespace, "namespace", "", "Kubernetes namespace (required)")
	rootCmd.Flags().BoolVar(&force, "force", false, "Force overwrite of the output file if it exists")
	rootCmd.Flags().BoolVar(&dryRun, "dry-run", false, "Dry run mode")

	_ = rootCmd.MarkFlagRequired("context")
	_ = rootCmd.MarkFlagRequired("namespace")

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.sops2sealedsecret.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".sops2sealedsecret" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".sops2sealedsecret")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

func execute(cmd *cobra.Command, args []string) {
	fromSopsFile := args[0]
	toSealedSecretFile := args[1]

	// Check if authorized to the given Kubernetes context
	if !helpers.IsAuthorized(k8s_context) {
		log.Fatalf("Not authorized to the given Kubernetes context: %s", k8s_context)
	}

	// Check if both files exist and are accessible/readable
	if !helpers.FileExistsAndReadable(fromSopsFile) {
		log.Fatalf("File %s does not exist or is not readable", fromSopsFile)
	}

	if fromSopsFile == toSealedSecretFile {
		log.Fatalf("Input and output files are the same")
	}

	if helpers.FileExistsAndReadable(toSealedSecretFile) {
		if !force {
			log.Fatalf("Output file %s already exists. Use --force to overwrite", toSealedSecretFile)
		}
		log.Printf("Warning: Overwriting existing file %s", toSealedSecretFile)
	}

	// Decrypt with sops and kubeseal
	sops2sealedsecret.DecryptAndSeal(fromSopsFile, toSealedSecretFile, k8s_context, namespace, dryRun)

	log.Printf("Successfully reencrypted from SOPS to SealedSecret. Context: %s, Namespace: %s, From: %s, To: %s", k8s_context, namespace, fromSopsFile, toSealedSecretFile)
}
