/*
Copyright © 2023 Christoph Görn <goern@b4mad.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"

	"codeberg.org/goern/sops2sealedsecret/pkg/sops2sealedsecret"
	"github.com/spf13/cobra"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version of sops2sealedsecret",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Version: %s\n", sops2sealedsecret.GetVersion())
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
