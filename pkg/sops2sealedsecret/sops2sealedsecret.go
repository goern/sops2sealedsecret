/*
Copyright © 2023 Christoph Görn <goern@b4mad.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package sops2sealedsecret

import (
	"log"
	"os"
	"os/exec"
)

// DecryptAndSeal decrypts the given file with sops and seals it with kubeseal, for a specified context and namespace.
func DecryptAndSeal(from, to string, k8s_context string, namespace string, dryRun bool) {
	// cmd_kubeseal holds the array of strings as an input to exec.Command()
	cmd_kubeseal := []string{"--context", k8s_context, "--namespace", namespace, "--controller-namespace", "sealed-secrets", "--format", "yaml"}

	if dryRun {
		log.Printf("Dry run mode. Not executing: sops %s | kubeseal %s", from, cmd_kubeseal)
	} else {
		// Decrypt with sops
		cmd1 := exec.Command("sops", "-d", from)

		// Kubeseal
		cmd2 := exec.Command("kubeseal", cmd_kubeseal...)
		cmd2.Stdin, _ = cmd1.StdoutPipe()

		// Redirect output to file
		outfile, err := os.Create(to)
		if err != nil {
			log.Fatalf("Failed to create output file: %v", err)
		}
		defer outfile.Close()
		cmd2.Stdout = outfile

		err = cmd2.Start()
		if err != nil {
			log.Fatalf("Failed to start kubeseal: %v", err)
		}

		err = cmd1.Run()
		if err != nil {
			log.Fatalf("Failed to run sops: %v", err)
		}

		err = cmd2.Wait()
		if err != nil {
			log.Fatalf("Kubeseal failed: %v", err)
		}
	}
}
