package helpers

import (
    "os"
)

func FileExistsAndReadable(filename string) bool {
    _, err := os.ReadFile(filename)
    return err == nil
}
