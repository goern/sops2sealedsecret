package helpers

import (
    "context"
    "log"
    "path/filepath"

    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/client-go/kubernetes"
    "k8s.io/client-go/tools/clientcmd"

    "k8s.io/client-go/rest"
)

// IsAuthorized checks if the user is authorized to the given Kubernetes context
func IsAuthorized(ctx string) bool {
    var config *rest.Config
    var err error

    // use the default kubeconfig file location
    config, err = clientcmd.BuildConfigFromFlags("", filepath.Join(HomeDir(), ".kube", "config"))

    if err != nil {
        log.Fatalf("Failed to create Kubernetes client config: %v", err)
    }

    clientset, err := kubernetes.NewForConfig(config)
    if err != nil {
        log.Fatalf("Failed to create Kubernetes client: %v", err)
    }

    // check if the user is authorized to the context
    _, err = clientset.CoreV1().Namespaces().List(context.TODO(), metav1.ListOptions{})
    return err == nil
}
