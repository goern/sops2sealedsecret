package helpers

import (
    "os"
)

// HomeDir returns the home directory of the current user
func HomeDir() string {
    if h := os.Getenv("HOME"); h != "" {
        return h
    }
    return ""
}
